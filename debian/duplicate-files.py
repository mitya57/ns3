#!/usr/bin/env python
import sys
import os
import os.path

md5_file=sys.argv[1]

f=open(md5_file)
c=f.readlines()
print "File count before deduplicate: %s." % (len(c))
removed_count=0
pre=['', '']
for i in c:
	l=i.strip().split('  ')
	if pre[0] == l[0]:
		os.remove(l[1])
		os.symlink(pre[1], l[1])
		removed_count+=1
	else:
		pre = l

f.close()
print "We removed %s files" % (removed_count)
